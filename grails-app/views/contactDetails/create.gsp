<g:each in="${contactDetails}" var="details">
    <g:render template="form" model="[details: details]"/>
</g:each>
<g:render template="form"/>

<script>
    function validateForm() {
        var fields = document.querySelectorAll('input[name^="mobile"], input[name^="phone"], input[name^="email"], input[name^="website"], input[name^="address"]');

        for (var i = 0; i < fields.length; i++) {
            if (fields[i].value.trim() !== "") {
                return true;
            }
        }
        alert("Preencha pelo menos um campo de detalhe de contato!");
        return false;
    }
    document.querySelector('form').onsubmit = validateForm;
</script>